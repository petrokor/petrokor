# Introduction
Hello, I'm Petro! 👋 

I am a Backend Engineer and I currently live in [Kyiv, Ukraine](https://en.wikipedia.org/wiki/Kyiv). This README should help you learn about me and the ways I work and how best to work with me.


# Background

I'm working in Tech since 2010. Ruby is one of my main tools since the very
beginning of my carrier. During the weekends I'm [performing in the playback theatre](https://dou.ua/lenta/articles/dou-hobby-playback/) in Kyiv and [other cities of Ukraine](https://www.facebook.com/MalePlaybackUA). I've been worked with [kottans.org community](https://youtube.com/playlist?list=PLEK9H5bICxvoiDKQ7epRpxUDmQotmvsgM) in the past and now considering to resume that mentoring activities. My favorite sport is squash. [Feel free to schedule some talk with me](https://calendly.com/petrokoriakin/talk-to-petro?month=2022-12)

# How to collaborate with Petro

I'm a feedback-driven and context-addicted personality. I tend to write a lot to make sure no important details are missed. When working on a project, my creativity and my ability to find smart workarounds can usually propel it the most. On the other hand, I dislike following traits of mine: overthinking, freeze-the-world syndrome, imposter syndrome. I welcome direct, actionable feedback from customers, co-workers, code review comments, and even RSpec telling me the specs are failing.

When reaching out to me, my preferences are:
- Tag me in a merge request
- Tag me in an issue
- Tag me in a message in a Teams channel
- Send me a 1:1 message in Teams
- Send me an email

# What does my typical working day look like?

I'm in [EET, UTC+2 or EEST, UTC+3](https://en.wikipedia.org/wiki/Eastern_European_Time) and here are some details:
- 8am - waking up, have a breakfast, walk through social media, sloowly heading out to the office space nearby.
- ~9am - starting working activities.
- 2pm or so - lunch and loong walk. Maybe some out-of-work appointments.
- 6pm - wrap up my day, switching into nonworking mode. heading out from the office space

_A note: I tend to practice late stays and over-the-weekend learning. It may look a bit unhealthy... but it is not for my current setup 😇_
